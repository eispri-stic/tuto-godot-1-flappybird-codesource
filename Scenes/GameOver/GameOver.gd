extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$ButtonRejouer.connect("pressed", self, "_on_btn_rejouer_pressed")


func _on_btn_rejouer_pressed():
	get_tree().change_scene("res://Scenes/MainScene/MainScene.tscn")
