extends Node2D


var jump_force = 0.0

const MAX_HEIGHT = 500
const MIN_HEIGHT = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	jump_force -= 350 * delta
	jump_force = max(0.0, jump_force)
	position.y += (Globals.gravity - jump_force) * delta
	
	position.y = min(MAX_HEIGHT, max(MIN_HEIGHT, position.y))



func _input(event):
	if Input.is_action_just_pressed("jump"):
		jump_force = 400


func _on_Hitbox_body_entered(body):
	get_tree().change_scene("res://Scenes/GameOver/GameOver.tscn")
