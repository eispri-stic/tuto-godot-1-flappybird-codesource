extends Node2D


var timer = 0.0
var spawn_rate = 2.0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if timer > spawn_rate:
		var obstacle = load("res://Scenes/Obstacle/Obstacle.tscn").instance()
		$SpawnObstacle.add_child(obstacle)
		timer = 0.0
	
	timer += delta
