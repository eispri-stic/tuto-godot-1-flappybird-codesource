extends StaticBody2D


var speed = 300


# Called when the node enters the scene tree for the first time.
func _ready():
	position.y += Globals.rng.randf_range(-150.0, 150.0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x -= speed * delta
